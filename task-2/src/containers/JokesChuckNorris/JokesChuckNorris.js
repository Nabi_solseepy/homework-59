import React, {Component} from 'react';
import Jokes from "../../component/Jokes/Jokes";

class JokesChuckNorris extends Component {
    state = {
      jokes: []
    };

    jokeHandler = () => {
        fetch('https://api.chucknorris.io/jokes/random').then(response =>{
            if (response.ok){
                return response.json();
            }
            throw new  Error('Something wrong with network request');
        }).then(joke => {
              const  jokes = [...this.state.jokes, joke];
            this.setState({jokes: jokes })
            console.log(this.state.jokes)
            });
    };
    render() {
        return (
            <div>
                <button onClick={this.jokeHandler}>submit</button>
                {this.state.jokes.map((joke, id) => (
                    <Jokes
                        key={id}
                     value={joke.value}
                    />
                ))}

            </div>
        );
    }
}

export default JokesChuckNorris;