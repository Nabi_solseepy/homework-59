import React, { Component } from 'react';

import './App.css';
import AddForm from "./component/AddForm/AddForm";
import Task from "./component/Task/Task";


class App extends Component {
  state = {
      items: [],
      value: '',
      Count: 0,
      currentTask : 0
  };


  addItem = () =>{
    let newItem = {name: this.state.value, count: this.state.Count + 1, currentTask: this.state.currentTask + 1};

    let items = [...this.state.items, newItem];

     this.setState({items: items, Count: this.state.Count + 1, value: '',  currentTask: this.state.currentTask  + 1})

  };

  inputVulue = (value) => {
    this.setState({value})
  };

  itemRemove = (name) => {
    let  items = [...this.state.items];
     let index = items.findIndex((item) => item.name === name );
      items.splice(index, 1);

      this.setState({
          items: items,
          Count: this.state.Count -1,
          currentTask: this.state.currentTask - 1
      })
  };
  moveChage = (name, id)=> {
     const  moveis = this.state.items;
     const movie = moveis[id].name = name;
     this.setState({items: moveis})

  };
  render() {
    return (
      <div className="App">
      <AddForm value={this.state.value} changed={this.inputVulue} addMovie={this.addItem}/>

          {this.state.items.map((item, id) => <Task name={item.name} changeHandler={(e) => this.moveChage(e.target.value, id)} key={id} count={item.count} removed={this.itemRemove} id={item.id}/>)}

      </div>
    );
  }
}

export default App;
