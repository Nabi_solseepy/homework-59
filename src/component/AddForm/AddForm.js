import React from 'react';
import './AddForm.css'
const AddForm = (props) => {
    return (
        <div>
            <input placeholder="text" type="text" className="Add" value={props.value} onChange={(e) => props.changed(e.target.value)}/>
            <button type="button" className="btn"  onClick={props.addMovie}>Add</button>
        </div>
    );
};

export default AddForm;