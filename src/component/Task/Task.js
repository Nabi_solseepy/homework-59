import React, {Component} from 'react';
import './Task.css';


class Task extends Component {

    constructor(props) {
        super(props)
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        return nextProps.name !== this.props.name
    }

    render() {
        return (
            <div>
                <input type="text" className="inp" value={this.props.name} onChange={this.props.changeHandler}/>
                <button type="button" className="btn-remove" onClick={()=> this.props.removed(this.props.name)}>X</button>
            </div>
        );
    }
}

export default Task;
